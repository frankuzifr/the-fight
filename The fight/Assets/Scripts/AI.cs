﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class AI : MonoBehaviour
{
    //скрипт для персонажей
    private NavMeshAgent navmesh;
    public GameObject bullet, bulletSpawn, aim;
    private GameObject bullets;
    public float visRadius, attackRadius;
    public int hp;
    private Vector3 enemyPos;
    private float distance;
    private string enemyTag;
    void Start()
    {
        navmesh = GetComponent<NavMeshAgent>();
        //выясняем, кто для нас является врагом
        if (gameObject.tag == "RedTeam")
        {
            enemyTag = "BlueTeam";
        }
        else if(gameObject.tag == "BlueTeam")
        {
            enemyTag = "RedTeam";
        }
    }

    void FixedUpdate()
    {
        Moving();
    }

    //функция для передвижения персонажей
    private void Moving()
    {
        Collider[] enemys = Physics.OverlapSphere(transform.position, visRadius); //считываем все объекты в радиусе visRadius и записываем в массив
        distance = 500f;
        for (int i = 0; i < enemys.Length; i++)
        {
            if (enemys[i].gameObject.tag == enemyTag) //выясняем какой объект является врагом
            {
                if (Vector3.Distance(transform.position, enemys[i].transform.position) < distance) //находим ближайшего к нам врага
                {
                    distance = Vector3.Distance(transform.position, enemys[i].transform.position); //сохраняем дистанцию ближайшего врага для дальнейших сравнений
                    navmesh.destination = enemys[i].transform.position; //двигаемся в позитицию ближайшего врага
                    if (Vector3.Distance(transform.position, enemys[i].transform.position) <= attackRadius) //если дистанция до врага меньше или равна attackRadius, то начинаем атаковать
                    {
                       Shoot();
                    }
                }
            }
        }
        if (hp <= 0) //смотрим на показатели жизней персонажа
        {
            Destroy(gameObject);
        }
    }

    //функция атаки
    private void Shoot()
    {
        bullets = Instantiate(bullet, bulletSpawn.transform.position, Quaternion.identity); //создание пуль
        bullets.transform.position = Vector3.Lerp(bullets.transform.position, aim.transform.position, 0.5f); //позиция, куда летит пуля
        if (bullet.transform.position == aim.transform.position) //уничтожаем пулю, когда она прилетела в место назначения
        {
            Destroy(bullet);
        }
    }

    //функция для уменьшения жизней при попадании пули
    private void OnCollisionEnter(Collision collision)
    {
        if ((collision.gameObject.tag == "BulletRed" && gameObject.tag == "BlueTeam") || (collision.gameObject.tag == "BulletBlue" && gameObject.tag == "RedTeam"))
        {
            hp--;
        }

    }
}



        
    

