﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System;

public class GameController : MonoBehaviour
{
    //скрипт для управления игрой, стартом игры и вычисления результата игры
    public Canvas startCanvas, timerCan;
    public GameObject[] blueTeam, redTeam;
    public Text winnerText, timer, timerEndText, lastTime, lastWinner;
    public Canvas endCanvas;
    private bool wasStart;
    private List<Vector3> redTeamSpawn = new List<Vector3> //добавляем места спавнов для персонажей красной команды
    {
        new Vector3(-98.5f, 5.88f, 49.6f),
        new Vector3(-91f, 5.88f, 32.7f),
        new Vector3(-103.8f, 5.88f, 15.3f),
        new Vector3(-80.8f, 5.88f, 4.7f),
        new Vector3(-82f, 5.88f, -16.8f),
        new Vector3(-100.4f, 5.88f, -29),
        new Vector3(-78.2f, 5.88f, -35.4f),
        new Vector3(-101.8f, 5.88f, -48.8f)
    };

    private List<Vector3> blueTeamSpawn = new List<Vector3> //добавляем места спавнов для персонажей синей команды
    {
        new Vector3(97.3f, 5.88f, 49.6f),
        new Vector3(98.3f, 5.88f, 32.7f),
        new Vector3(100.1f, 5.88f, 15.3f),
        new Vector3(77.8f, 5.88f, 4.7f),
        new Vector3(79f, 5.88f, -16.8f),
        new Vector3(100, 5.88f, -29),
        new Vector3(86, 5.88f, -35.4f),
        new Vector3(103.2f, 5.88f, -48.8f)
    };
    
    void Start()
    {
        Time.timeScale = 0; //по началу ставим игру на паузу
    }

    void Update()
    {
        if (wasStart) //определяем, была ли нажата кнопка старт. Проверка необходима для того, чтобы вычисление результата не начиналось раньше времени
        {
            CalculationWin();
        }
    }
    
    //функция для начала боя
    public void PlayGame()
    {
        wasStart = true;
        startCanvas.gameObject.SetActive(false);
        timerCan.gameObject.SetActive(true);
        Time.timeScale = 1; //убираем паузу
        for (int i = 0; i < 3; i++)
        {
            int numR = UnityEngine.Random.Range(0, 8); //рандомно выбираем, какая позиция будет выбрана из заданного массива позиции для спавна персонажей красной команды
            int numB = UnityEngine.Random.Range(0, 8); //рандомно выбираем, какая позиция будет выбрана из заданного массива позиции для спавна персонажей синей команды
            List<Vector3> posT = new List<Vector3>(); //добавляем массив для хранения уже использованных позиции для спавна
            
            foreach (Vector3 vec in posT) //перебираем уже использованные позиции для спавна
            {
                while (vec == redTeamSpawn[numR]) //ищем позицию, которая не использовалась еще у красной команды
                {
                    numR = UnityEngine.Random.Range(0, 8);
                }
                while (vec == blueTeamSpawn[numB]) //ищем позицию, которая не использовалась еще у синей команды
                {
                    numB = UnityEngine.Random.Range(0, 8);
                }
            }
            posT.Add(redTeamSpawn[numR]); //добавляем использованную позицию красной команды в массив использованных позиции 
            posT.Add(blueTeamSpawn[numB]); //добавляем использованную позицию синей команды в массив использованных позиции 
            Instantiate(redTeam[i], redTeamSpawn[numR], Quaternion.identity); //создаем персонажа красной команды
            Instantiate(blueTeam[i], blueTeamSpawn[numB], Quaternion.identity); //создаем персонажа синей команды
        }
    }

    //функция для расчета результата
    private void CalculationWin()
    {
        int bt = 0, rt = 0; //добавляем счетчики, живый песронажей
        Collider[] enemys = Physics.OverlapSphere(transform.position, 600); //считываем все существующие объекты на сцене в радиусе 600 единиц
        for (int i = 0; i < enemys.Length; i++) 
        {
            if (enemys[i].gameObject.tag == "BlueTeam") //если объект принадлежит к синей команде, то прибавляем к счетчику синей команды +1
            {
                bt++;
            }
            else if (enemys[i].gameObject.tag == "RedTeam") //если объект принадлежит к красной команде, то прибавляем к счетчику синей команды +1
            {
                rt++;
            }
        }
        double time = Convert.ToDouble(timer.text);
        if (rt == 0 && bt == 0 || time >= 30) //если счетчики живых персонажей команд по нулям, значит живых нет, и объявляется ничья
        {
            EndGame("Nobody");
        }
        else if (rt == 0) //если счетчик живых персонажей красной команды равняется нулю, то значит, что красная команды проиграла
        {
            EndGame("Blue");
        }
        else if (bt == 0) //если счетчик живых персонажей синей команды равняется нулю, то значит, что красная команды проиграла
        {
            EndGame("Red");
        }
    }

    //функция для определения кто выйграл
    private void EndGame(string winner)
    {
        Time.timeScale = 0; //останавливаем игру
        endCanvas.gameObject.SetActive(true);
        if (winner == "Red") //если победила красная команда, то будет выводится сообщение, что победила красная команда
        {
            winnerText.text = "Команда красных победила!";
            lastWinner.text = PlayerPrefs.GetString("Winner");
            PlayerPrefs.SetString("Winner", "В прошлом бою выйграла красная команда!");
            winnerText.color = Color.red;
        }
        else if (winner == "Blue") //если победила синяя команда, то будет выводится сообщение, что победила синяя команда
        {
            winnerText.text = "Команда синих победила!";
            lastWinner.text = PlayerPrefs.GetString("Winner");
            PlayerPrefs.SetString("Winner", "В прошлом бою выйграла синяя команда!");
            winnerText.color = Color.blue;
        }
        else if (winner == "Nobody") //если никто не победил, то будет выводится сообщение, что никто не победил
        {
            winnerText.text = "Ничья";
            lastWinner.text = PlayerPrefs.GetString("Winner");
            PlayerPrefs.SetString("Winner", "В прошлом бою никто не выйграл!");
            winnerText.color = Color.black;
        }
        timerEndText.text = "Время текущего боя: " + timer.text; //берем из таймера время, которое прошло от начала боя
        lastTime.text = "Время прошлого боя: " + PlayerPrefs.GetString("LastFight"); //берем время прошлого боя
        PlayerPrefs.SetString("LastFight", timer.text); //записываем время текущего боя вместо предыдущего, чтобы в следующем бою вывелось вромя этого боя
        timer.gameObject.SetActive(false);
        gameObject.SetActive(false);
    }

    //при нажатии на кнопку рестарт перезагружаем сцену
    public void Restart()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }
}
