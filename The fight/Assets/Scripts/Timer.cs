﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Timer : MonoBehaviour
{
    //скрипт таймера в верхнем левом углу
    public Text timer;
    private float timerStart;
    void Start()
    {
        timer.text = timerStart.ToString("F2");
    }

    void Update()
    {
        timerStart += Time.deltaTime;
        timer.text = timerStart.ToString("F2");
    }
}
