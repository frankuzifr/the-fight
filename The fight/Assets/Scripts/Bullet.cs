﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{
    //скрипт для уничтожения пули через определнное кол-во времени
    private void Update()
    {
        StartCoroutine(DeleteBullet());
    }

    IEnumerator DeleteBullet()
    {
        yield return new WaitForSeconds(0.4f);
        Destroy(gameObject);
    }
}
